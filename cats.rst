.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

Context-Aware Touch-Screen Blueprint
####################################

.. contents::
   :depth: 4

Overview
********

The Context-Aware Touch-Screen (CATS) blueprint demonstrates a smart
touch-screen that is able to control multiple devices over a network. 
Wired communication over ethernet as well as wireless communication over Wi-Fi 
and low-power Thread mesh network.

A device manufacturer using Oniro can utilise a CATS panel in a one-to-many 
or one-to-one design to allow a standardised touch panel using a defined API 
to be easily integrated into their products, yet control others.

With extensibility as a core principle of Oniro, CATS code is written in a such
a way that extending CATS is easy with modular virtual screens created using
the `Light and Versatile Embedded Graphics Library (LVGL) <https://lvgl.io/>`_.

This blueprint demonstrates the use-case in which the user controls a doorlock 
which is connected in the Thread network. 
The CATS panel displays the doorlock's keypad and sends the keys that are being
pressed to the doorlock which ultimately locks/unlocks the doorlock when the 
inserted PIN is correct.

Doorlock Blueprint Setup
------------------------
If you would like to try out the doorlock blueprint controlled by CATS, in 
addition to the current guide, follow the :doc:`doorlock blueprint 
documentation <doorlock>` to build the doorlock.

Hardware Setup
**************

Bill of Materials
-----------------
- 1x `Raspberry Pi 4B board 
  <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/>`_ (>=2GB RAM)
- 1x `Raspberry Pi USB-C Power supply 
  <https://www.raspberrypi.com/products/type-c-power-supply/>`_
- 1x microSD card (>=8GB size)
- 1x Arduino Nano 33 BLE
- 1x `USB serial cable <https://www.adafruit.com/product/954>`_ (optional)
- 1x `7inch HDMI LCD Display, Waveshare, Rev3.1 or Rev2.1
  <https://www.waveshare.com/wiki/7inch_HDMI_LCD_(C)>`_ (comes with USB and 
  HDMI cable)
- 1x HDMI to micro-HDMI adapter

The setup of the hardware components is illustrated in the hardware setup image:

.. image:: assets/cats-hardware-setup.jpg
   :alt: CATS Hardware setup

Connect the touch-screen display to the Raspberry Pi board:

- Connect the display's HDMI input through a micro-HDMI adapter to the 
  connector labeled HDMI0 on the Raspberry Pi 4B board (see the image above).
- Connect the display's power + touchscreen port to one of the Raspberry Pi 4B 
  board USB ports.

For the Raspberry Pi 4 you need to perform a few more steps for completing the 
setup:

- Insert the Arduino Nano 33 BLE in one of the USB ports this will act as 
  OpenThread Radio Co-Processor.
- Insert the micro SD card, and the USB-C power supply can wait until the 
  software is build and flashed.

Optionally, a USB to TTL serial cable can be connected as a serial console for 
debugging. A detailed description of connecting the cable can be found `on the 
connect the lead page 
<https://learn.adafruit.com/adafruits-raspberry-pi-lesson-5-using-a-console-cable/connect-the-lead>`_. 
An alternative to the serial console is to SSH over the local network 
connection.

Fetch Blueprint Sources and Build
*********************************

Fetch the Build Metadata
------------------------

For fetching or updating the build metadata, follow the :ref:`blueprints
workspace documentation <Workspace>`. Doing so, brings everything you need on 
your host to start building an image for this blueprint. For this specific 
build, you need to clone `meta-oniro-blueprints-cats 
<https://gitlab.eclipse.org/eclipse/oniro-blueprints/context-aware-touch-screen/meta-oniro-blueprints-cats>`_.

.. code-block:: console

   $ git clone --recurse-submodules https://gitlab.eclipse.org/eclipse/oniro-blueprints/context-aware-touch-screen/meta-oniro-blueprints-cats.git

.. note::

   Make sure you define or replace `CHECKOUT_DIR` as described in 
   :ref:`blueprints workspace documentation <Workspace>`.


Build the Oniro Image for CATS
-------------------------------------

Once you have a workspace initialized as per the instructions in the preceding 
section, you are ready to build the OS. Firstly, you initialize a build 
directory:

.. code-block:: console

   $ cd $CHECKOUT_DIR
   $ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux

Executing the command sets up a new build environment in the 
`build-oniro-linux` directory (or reuse it, if it already exists).

Next we add the blueprint layers, this is only needed once after initializing 
a new build:

.. code-block:: console

   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints-cats/meta-oniro-blueprints-core
   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints-cats/meta-oniro-blueprints-cats

Once the build environment is set, start building the image.

.. code-block:: console

   $ DISTRO="oniro-linux-blueprint-cats" MACHINE=raspberrypi4-64 bitbake blueprint-cats-gateway-image

Once the build finishes, the image is ready to be flashed on a microSD card.

Flash
-----

Ensure that you have plugged in an microSD card via an microSD card reader 
attached to your host. Once that is done, note its associated device node. 
You can find that using `udev`, `dmesg` or various other tools. Once the device 
node associated to the microSD card is known, proceed to flash the built image.

.. warning::

   The following commands assume that the device node associated with the
   microSD card is provided via the `DEVICE` environment variable. Make sure
   you have the correct one set before running the commands to avoid
   risking the loss of data.

   The image to flash is a full disk image, so the `DEVICE` variable needs to
   point to the entire block device node, and not to a specific partition.

.. code-block:: console

   $ sudo bmaptool copy ./tmp/deploy/images/raspberrypi4-64/blueprint-cats-gateway-image-raspberrypi4-64.wic.gz "$DEVICE"

.. note::

   If the device that needs to be flashed had existing partitions, you may
   need to unmount the existing partitions using the `umount` command. 

Prepare OpenThread Radio Co-Processor (RCP)
------------------------------------------------------
Build a OpenThread radio co-processor firmware for the Arduino Nano 33 BLE, this 
will act as a Thread radio for the Raspberry Pi board. 

To build the RCP image initialize a new build environment using the Zephyr 
build flavour:

.. code-block:: console

   $ TEMPLATECONF=../oniro/flavours/zephyr . ./oe-core/oe-init-build-env build-oniro-zephyr

Run the following command from the newly created build environment:

.. code-block:: console

   $ MACHINE=arduino-nano-33-ble DISTRO=oniro-zephyr bitbake zephyr-openthread-rcp

After building the image, we can now flash the Arduino board. Prepare the board 
by connecting the board's USB port to your computer and putting the board into 
flashing mode by double-pressing the reset button.

Next, use the following command to flash the board:

.. code-block:: console

   $ MACHINE=arduino-nano-33-ble DISTRO=oniro-zephyr bitbake zephyr-openthread-rcp -c flash_usb

Once flashed the Arduino board you can connect it into one of the USB ports on 
the Raspberry Pi board.

.. note::

   The *nRF52840 DK* and *nRF52840 Dongle* are alternative boards that can also 
   be used as RCP. In order to build and flash one of these boards, please 
   replace the `MACHINE` variable to ``nrf52840dk-nrf52840`` or 
   ``nrf52840-mdk-usb-dongle`` accordingly.  

Use CATS
********
At this point, we have the hardware ready and we can run the blueprint. 

Plug in the microSD card on which you flashed the OS into the board's microSD
card slot. That brings us to the last step: booting the board by attaching the
power source. On a Raspberry Pi 4B, that would be a USB-C power supply.

When the CATS app loads, it presents the main screen with Oniro logo. The user
can swipe left and right to traverse a set of screens.

You will be able to switch between the following screens:

* main: simple screen with Oniro logo.
* keypad: numeric keypad screen which can be used to control the doorlock.
* demo: includes the demo screen that showcases animations and widgets.
* settings: screen with setting options for example to change the light/dark 
  theme.
* Developer settings: screen containing developer options.

Doorlock Zephyr Node Onboarding
-------------------------------
If you completed the :doc:`doorlock blueprint setup <doorlock>`, before we can 
control the doorlock with CATS we have to onboard the doorlock Zephyr node in 
the OpenThread network. 
Follow the instructions in the `transparent gateway documentation 
<transparent-gateway.html#run-the-gateway-blueprint>`_ to onboard the 
mesh node. 

Once you completed the onboarding process you will be able to control the 
doorlock using CATS. First of all, using the keypad screen in CATS, the first 
four digits you enter will be used as the new PIN and then using the same PIN 
you can lock/unlock the doorlock. 
See the doorlock blueprint documentation for more information.

Resources
*********

.. _ResourcesTG:

- `Demo video of CATS blueprint used to operate a safe box 
  <https://www.youtube.com/watch?v=1SxcqvR_QUI>`_
